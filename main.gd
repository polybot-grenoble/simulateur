extends Node2D

func _ready():#execute lors que tout les noeuds sont initialisé (erreur si on utilise la fonction _init())
	var shape=($VinyleTable2024FinalV1/Area2D/CollisionShape2D.shape.size)#taile de la table 
	#mise a l'echelle en fonction de la taille de la fenêtre :
	shape=shape*$VinyleTable2024FinalV1.scale
	print(shape)#print de debug
	print($VinyleTable2024FinalV1.position)
	$robot.rotation_degrees = 130
	$robot.start(shape,$VinyleTable2024FinalV1.position)#apelle la fonction start du robot

func _process(delta):
	#pareil qu'en haut (j'aurais peut-être du faire une fonction)
	var shape=(($VinyleTable2024FinalV1/Area2D/CollisionShape2D.shape.size))
	shape=shape*$VinyleTable2024FinalV1.scale
	$robot.start(shape,$VinyleTable2024FinalV1.position)
	$VBoxContainer/Angles/degre/Label.text = str("%.3f" % $robot.rotation_degrees,"°")
	$VBoxContainer/Angles/rad/Label.text = str("%.3f" % $robot.rotation,"rad")
	$VBoxContainer/HBoxContainer/x/Label.text = str("x = ", int($robot.toMM($robot.toTable($robot.posT,$robot.size),$robot.size).x))
	$VBoxContainer/HBoxContainer/y/Label.text = str("y = ", int(14.243613 + $robot.toMM($robot.toTable($robot.posT,$robot.size),$robot.size).y))

func _on_button_3_pressed(): #changer le fichier
	$Control.nouveaufich()
	pass # Replace with function body.

func _on_rad_text_submitted(new_text):
	$VBoxContainer/Angles/rad.clear()#vide la ligne
	pass # Replace with function body.

func _on_degre_text_submitted(new_text):
	$VBoxContainer/Angles/degre.clear()#vide la ligne
	pass # Replace with function body.


func _on_y_text_submitted(new_text):
	$VBoxContainer/HBoxContainer/y.clear()#vide la ligne
	pass # Replace with function body.
	

func _on_x_text_submitted(new_text):
	$VBoxContainer/HBoxContainer/x.clear()#vide la ligne
	pass # Replace with function body.


func _on_x_robot_text_submitted(new_text):
	$VBoxContainer/coord_robot/x_robot.clear()#vide la ligne
	pass # Replace with function body.

func _on_y_robot_text_submitted(new_text):
	$VBoxContainer/coord_robot/y_robot.clear()#vide la ligne
	pass # Replace with function body

func _on_delta_deg_text_submitted(new_text):
	$VBoxContainer/Angles/delta_deg.clear()#vide la ligne
	pass # Replace with function body.

func _on_delta_rad_text_submitted(new_text):
	$VBoxContainer/Angles/delta_rad.clear()#vide la ligne
	pass # Replace with function body.
