extends Button
var mode="souris"
signal mode_s(md)

# Called when the node enters the scene tree for the first time.
func _ready():
	text="mode deplacement\n"+mode
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	mode_s.emit(mode)
	pass

func _on_pressed():
	if mode == "souris":
		mode = "clavier"
	else:
		mode = "souris"
	text="mode deplacement\n"+mode
	pass # Replace with function body.
