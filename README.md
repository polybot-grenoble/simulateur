# Manuel utilisation simulateur

## Lancement projet
Pour lancer le simulateur, lancer Godot_v4.1.2-stable_win64.exe présent dans le dossier. Cliquer sur Importer (à droite de la console), et importer le project.godot du répertoire du projet. Le projet met du temps à se charger, c'est normal. Pour lancer la simulation, cliquer sur l'icône play (triangle en haut à droite) qui lance la simulation du projet. Si vous changer le code, il faut reload la simulation en cliquant au même endroit (recharger la scène jouée)

## Infos importantes

ATTENTION LES ROTATIONS DU ROBOT SE FONT DANS LE SENS INVERSE DANS LA REALITE (VRAI VIE = SENS TRIGO, SUR CETTE SIMU = INVERSE SENS TRIGO), IL FAUT DONC MULTIPLIER PAR -1 POUR PASSER DES STRATS A LA SIMU OU DE LA SIMU AUX STRATS.

## Description des différents paramètres
_ L'origine de la table se situe en bas à gauche.
_ x=... et y=... affichent les coordonnées actuelles du robot en mm sur le terrain (comme dans la réalité, oui les vrais mm)
_ ...° et ...rad affichent l'orientation du robot par rapport à son origine (0°).


### Affichages des différents paramètres
### Déplacer (sans précision) le robot avec le mode souris
Lorsque vous lancez la simulation, le mode par défault est le déplacement par souris. Si vous avez changé entre temps, cliquez sur le boutton mode déplacement clavier. Dans le mode souris, vous pouvez également utiliser les flèches du clavier pour dépplacer légèrement les robot selon les coordonnées du terrain.

### Déplacer (sans précision) le robot avec le mode clavier
Cliquez sur le boutton mode dépklacement souris pour changer de mode. Vous pouvez utiliser ce mode pour piloter le robot en le faisant avancer/reculer avec les flèches haut/bas et en changeant l'orientation en utilisant les flèches du côtés.

### Positionner à des coordonnées précises du terrain
Utiliser les 2 cases en dessous de x=... et y=...

### Déplacer le robot selon son axe x et y
Le robot dispose de ses propres axes x et y (barres bleues sur l'image du robot). Pour effectuer un deplacement selon ses axes, il faut utiliser les 2 cases en dessous de x_robot et y_robot. Utiliser un - pour reculer selon ces axes.

### Imposer une angle du robot par rapport à son 0
Pour cela, utilisez les cases juste en dessous de l'info des angles, à gauche si vous le donnez en ° et à droite si c'est en rad.

### Faire varier l'angle du robot par rapport à son orientation actiuelle
Pour cela, utilisez les cases juste au dessus de delat° et delta rad, à gauche si vous le donnez en ° et à droite si c'est en rad.

### Boutton reset les coordonnées de départ
Pour reset les coordonnées de départ ainsi que l'orientation cliquez sur le boutton reset coordonnées. Vous pouvez changer les coordonnées et l'orientation dans le code dans le fichier robot.gd, aux alentours de la ligne 127 (func _on_button_5_pressed():#reset les coordonnées).

