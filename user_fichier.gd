extends Control
var pt=""
var coco
var rot=0
var rtn=0
var premier = true
var reset=false
@onready var fd = $FileDialog
# Called when the node enters the scene tree for the first time.
signal chemin(pt)

func _ready():
	fd.visible=false
	var d = "res://Coordonnees/"
	fd.current_dir=d
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func nouveaufich():
	fd.visible=true


func _on_file_dialog_file_selected(path): #un fichier est selectionner sur la fenetre des fichier
	var f
	if FileAccess.file_exists(path):#si il exist on l'ouvre pour le fun
		f = FileAccess.open(path,FileAccess.READ)
		f.close()
	else :#sinon on cree le fichier
		f = FileAccess.open(path,FileAccess.WRITE)
		f.close()
	pt=path#stock le chemein d'acces
	chemin.emit(pt)
	#_on_robot_load(coco)
	pass # Replace with function body.


func _on_robot_load(coord):
	if pt=="":#si il n'y a pas de chemin d'acce on ouvre la fenetre de selection de fichier
		coco=str(coord)+"\n"
		fd.visible=true#ouverture de la fenetre
	else:#sinon on sauvegarde les coordonne a la suite du fichier
		var f
		var txt#tableau qui contient une ligne du fichier
		var vtxt=[]#tableau qui contient les tableau de ligne
		
		f = FileAccess.open(pt,FileAccess.READ) #lecture du fichier
		txt="kk"
		vtxt=[]
		while f.get_position()<f.get_length():#parcour du fichier du début jusqu'a la fin
			txt=f.get_csv_line(";")
			#txt.pop_back()
			vtxt.append(txt) #stock les lignes dans vtxt
		f.close()
		f = FileAccess.open(pt,FileAccess.WRITE)#ouvre le fichier pour ecrire dedans
		coco=[]#nouvelle coordonne a rajouter
		coco.append(str(int(round(coord.x))))
		#coco.append(str((coord.x)))
		#coco.append(str(coord.y))
		coco.append(str(int(round(coord.y))))#ajout de x et y
		if !vtxt.is_empty():#si le fichier n"'est pas vide
			for i in range(len(vtxt)):
				f.store_csv_line(vtxt[i],";")#stock les lignes precedentes dans le fichier (pour pas les perdre)
			if premier:#si c'est la premiere valeur de rotation
				coco.append(str(0))
			else:
				coco.append(str(rtn-rot))#ajoute la rotation on fonction de la rotation precedente (decalage en °)
		else:#sinon rien a faire et la rotation est a 0
			coco.append(str(0))#ajoute rotation (0)
		coco.append("")#rajout du point virgule de fin
		f.store_csv_line(coco,";")#stock les nouvelles coordonnées
		rot=rtn #la valeur precendent de rotation prend la nouvelle
		f.close()
		premier=false
	pass # Replace with function body.



func _on_confirmation_dialog_confirmed():#reset le fichier
	premier = true
	if FileAccess.file_exists(pt):#si le fichier exist le reset
		var f = FileAccess.open(pt,FileAccess.WRITE)
		f.close()
	pass # Replace with function body.


func _on_robot_rt(rota):#recupere la rotation du robot
	rtn=rota
	pass # Replace with function body.
