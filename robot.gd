extends CharacterBody2D
signal load(coord)#est emis pour load les coordonnee
signal rt(rot) #utilise pour transmettre la rotation du robot (en degré)
var dragging = false
var click_radius = 80 # Size of the sprite.
var size #taille de la table
var posT #position de la table
var posRsT #position du robot sur la table
var MMR#position du robot en mm
var mode="souris"
var speed = 500
var angular_speed = 360
var dir = Vector2.ZERO #déplacement x et y du robot selon ses propres directions

func _process(delta):
	if(rotation_degrees>=360):
		rotation_degrees -= 360
	if (rotation_degrees<0):
		rotation_degrees += 360	
		
	if dir != Vector2.ZERO:
		if (rotation_degrees >= 0) && (rotation_degrees < 90):
			position.x += tocoord_x(sin((PI/2)-rotation)*dir.x + sin(rotation)*dir.y, size)
			position.y += tocoord_y(cos((PI/2)-rotation)*dir.x - cos(rotation)*dir.y ,size)
		elif(rotation_degrees >= 90) && (rotation_degrees < 180):
			position.x += tocoord_x(-cos(PI-rotation)*dir.x + cos(rotation-(PI/2))*dir.y, size)
			position.y += tocoord_y(sin(PI-rotation)*dir.x + sin(rotation-(PI/2))*dir.y, size)
		elif(rotation_degrees >= 180) && (rotation_degrees < 270):
			position.x += tocoord_x(- sin((PI/2)-(rotation-(PI)))*dir.x - sin(rotation-(PI))*dir.y, size)
			position.y += tocoord_y(-cos((PI/2)-(rotation-(PI)))*dir.x + cos(rotation-(PI))*dir.y, size)
		else:
			position.x += tocoord_x(cos((PI/2)-(rotation-((3*PI)/2)))*dir.x - cos(rotation-((3*PI)/2))*dir.y, size)
			position.y += tocoord_y(-sin((PI/2)-(rotation-((3*PI)/2)))*dir.x -sin(rotation-((3*PI)/2))*dir.y, size)
	dir = Vector2.ZERO
	
	if mode=="clavier": #mode de deplacement avec touches
		var direction = 0
		var velocity = Vector2.ZERO
		if Input.is_action_pressed("ui_left"):
			direction = -1
		if Input.is_action_pressed("ui_right"):
			direction = 1
		if Input.is_action_pressed("ui_up"):
			velocity = Vector2.UP.rotated(rotation) * speed
		if Input.is_action_pressed("ui_down"):
			velocity= -1 * Vector2.UP.rotated(rotation) * speed
		rotation_degrees += angular_speed * direction * delta
		
		position += velocity * delta
		#position.x = clamp(position.x,0,11339)
		#position.y = clamp(position.y,0,7559)
		#var trobot=Vector2(60,60)
		#position=position.clamp(posT-size/2+trobot,size/2+posT-trobot)
		#position=position.clamp(posT-size/2,size/2+posT)#pour pas sortir du terrain 
	rt.emit(rotation_degrees)#emet la rotation en continue
	
func start(s,po): #récupère la taille et la position de la table
	size=s
	posT=po
	
func toTable(posT,size):#converti la position du robot de la fenetre a la position du robot sur la table
	var newp
	var offset=posT
	newp=position-offset
	newp+=size/2
	newp.y=size.y-position.y #retournement axe y
	return newp

func toMM(posRsT,size):#converti en mm les coordonnées
	var MMR=(posRsT*Vector2(3000,2000))/(size)
	return MMR

func tocoord_x(x,size): #converti la position en mm sur le terrain de x vers une coordonnée pixel
	var coord_x= ((x*size)/Vector2(3000,2000)).x
	return coord_x
func tocoord_y(y,size): #converti la position en mm sur le terrain de y vers une coordonnée pixel
	var coord_y= ((y*size)/Vector2(3000,2000)).y
	return coord_y

func _input(event):
	if mode=="souris": #éxécuté quand l'utilisateur fait un truc
		if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT:#le bouton gauche de la souris est cliqué
			if (event.position - position).length() < click_radius:
				# Start dragging if the click is on the sprite.
				if not dragging and event.pressed:
					dragging = true
			# Stop dragging if the button is released.
			if dragging and not event.pressed:
				dragging = false
		if event is InputEventMouseMotion and dragging:
			# While dragging, move the sprite with the mouse.
			position += event.relative
		#pour déplacer avec le clavier :
		if Input.is_action_just_pressed("ui_down") :
			position.y+=1
		if Input.is_action_just_pressed("ui_up") :
			position.y-=1
		if Input.is_action_just_pressed("ui_right") :
			position.x+=1
		if Input.is_action_just_pressed("ui_left") :
			position.x-=1
	var trobot=Vector2(60,60)#taille du robot (je crois /2)
	#position=position.clamp(posT-size/2+trobot,size/2+posT-trobot) #empêche le robot de sortir de la table (de manière plus commune :positon.clamp(vetc2.ZERO,size[de la fenetre pour pas sortir de la fenêtre]))
	#formule a taton a revoir elle est surement fausse pour la position.clamp
func _on_button_pressed():#exécuté quand le bouton est pressé (sauv les coordonnées)
	posRsT=toTable(posT,size)
	print("la position du robot: ",position)
	print("la position sur la table est : ",posRsT)
	var psmm=toMM(posRsT,size)
	psmm.y+=14.243613#on regle le pb en mode bourin
	print("position en mm : ",psmm)
	load.emit(psmm)

func _on_vinyle_table_2024_final_v_1_exit():#execute lorsque le robot sort de la table (sert a rien)
	print("exiting !!!")



func _on_vinyle_table_2024_final_v_1_enter():#execute lorsque le robot rentre dans la table (sert a rien)
	print("enter !!!")


func _on_button_2_mode_s(md):#changement du mode
	mode=md
	pass # Replace with function body.

func _on_button_5_pressed():#reset les coordonnées
	position.x=267
	position.y=76
	rotation_degrees=130
	pass # Replace with function body.
	



func _on_x_text_submitted(new_text): #saisie coord x
	var i=0
	while (i<len(new_text)):
		if (new_text[i]<'0' ||  new_text[i]>'9') && new_text[i]!='-' && new_text[i]!='.':
			return
		i+=1
	position.x=(posT-size/2).x + (((float(new_text))*size)/Vector2(3000,2000)).x
	pass # Replace with function body.


func _on_y_text_submitted(new_text): #saisie coord y
	var i=0
	while (i<len(new_text)):
		if (new_text[i]<'0' ||  new_text[i]>'9') && new_text[i]!='-' && new_text[i]!='.':
			return
		i+=1
	position.y= (posT+size/2).y - (((float(new_text))*size)/Vector2(3000,2000)).y
	pass # Replace with function body.


func _on_degre_text_submitted(new_text): #saisie angle en degre
	var i=0
	while (i<len(new_text)):
		if (new_text[i]<'0' ||  new_text[i]>'9') && new_text[i]!='-' && new_text[i]!='.':
			return
		i+=1
	rotation_degrees = float(new_text)
	pass # Replace with function body.


func _on_rad_text_submitted(new_text): #saisie angle en rad
	var i=0
	while (i<len(new_text)):
		if (new_text[i]<'0' ||  new_text[i]>'9') && new_text[i]!='-' && new_text[i]!='.':
			return
		i+=1
	rotation = float(new_text)
	pass # Replace with function body.
	


func _on_x_robot_text_submitted(new_text): #saisie du deplacement x du robot selon ses propres axes
	var i=0
	while (i<len(new_text)):
		if (new_text[i]<'0' ||  new_text[i]>'9') && new_text[i]!='-' && new_text[i]!='.':
			return
		i+=1
	dir.x = float(new_text)

	pass # Replace with function body.

func _on_y_robot_text_submitted(new_text): #saisie du deplacement y du robot selon ses propres axes
	var i=0
	while (i<len(new_text)):
		if (new_text[i]<'0' ||  new_text[i]>'9') && new_text[i]!='-' && new_text[i]!='.':
			return
		i+=1
	dir.y = float(new_text)
	pass # Replace with function body.


func _on_delta_deg_text_submitted(new_text): #entree d'une variation de l'angle en degres
	var i=0
	while (i<len(new_text)):
		if (new_text[i]<'0' ||  new_text[i]>'9') && new_text[i]!='-' && new_text[i]!='.':
			return
		i+=1
	rotation_degrees += float(new_text)
	pass # Replace with function body.


func _on_delta_rad_text_submitted(new_text): #entree d'une variation de l'angle en rad
	var i=0
	while (i<len(new_text)):
		if (new_text[i]<'0' ||  new_text[i]>'9') && new_text[i]!='-' && new_text[i]!='.':
			return
		i+=1
	rotation += float(new_text)
	pass # Replace with function body.
